<?php

require_once __DIR__ . '/../vendor/autoload.php';

$kernel = \AspectMock\Kernel::getInstance();
$kernel->init([
    'debug' => true,
    'includePaths' => [
        __DIR__ . '/../services/',
    ],
    'cacheDir' => __DIR__.'/../runtime/aspectmock_cache',
]);
