<?php

use AspectMock\Test;
use services\{
    Comission,
    BinProvider,
    ExchangeRate,
};

class ComissionTest extends \Codeception\Test\Unit
{
    protected function _before(){
        Dotenv\Dotenv::createImmutable(__DIR__ . '../../..')->load();
    }

    /**
     * @dataProvider dataProviderTranslateTitles
     */
    public function testCalculate($entry, $expected)
    {
        Test::clean();
        Test::double(BinProvider::class, ['getCountryAlpha2' => function ($bin) {
            return [
                '45717360' => 'DK',
                '516793' => 'LT',
                '45417360' => 'JP',
                '41417360' => 'US',
                '4745030' => 'GB',
            ][$bin];
        }]);
        Test::double(ExchangeRate::class, [
            'getAllRates' => function(){
                return [
                    'EUR' => 1,
                    'USD' => 1.083988,
                    'JPY' => 136.48708,
                    'GBP' => 0.833039,
                ];
            }
        ]);

        $model = new Comission($entry);
        $result = $model->calculate();

        self::assertEquals($expected, $result);
    }

    public function dataProviderTranslateTitles(): array
    {
        return [
            [
                'entry' => ['bin' => '45717360','amount' => '100.00','currency' => 'EUR'],
                'expected' => 1
            ],
            [
                'entry' => ['bin' => '516793','amount' => '50.00','currency' => 'USD'],
                'expected' => 0.46
            ],
            [
                'entry' => ['bin' => '45417360','amount' => '10000.00','currency' => 'JPY'],
                'expected' => 1.46
            ],
            [
                'entry' => ['bin' => '41417360','amount' => '130.00','currency' => 'USD'],
                'expected' => 2.39
            ],
            [
                'entry' => ['bin' => '4745030','amount' => '2000.00','currency' => 'GBP'],
                'expected' => 48.01
            ],
        ];
    }
}
