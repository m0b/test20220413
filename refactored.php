<?php

require __DIR__ . '/vendor/autoload.php';

use services\Comission;

Dotenv\Dotenv::createImmutable(__DIR__)->load();

foreach (explode("\n", file_get_contents($argv[1])) as $row) {
    if (empty($row)) {
        break;
    }
    $row = json_decode($row, true);

    $comission = new Comission($row);
    $result = $comission->calculate();

    echo $result . "\n";
}
