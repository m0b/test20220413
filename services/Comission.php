<?php

namespace services;

class Comission
{
    private $bin;
    private $amount;
    private $currency;

    public function __construct(array $entry)
    {
        $this->bin = $entry['bin'];
        $this->amount = $entry['amount'];
        $this->currency = $entry['currency'];
    }

    public function calculate(): string
    {
        $rate = ExchangeRate::getRate($this->currency);

        $amountFixed = $this->amount;
        if ($rate > 0) {
            $amountFixed = bcdiv($amountFixed, $rate, 5);
        }

        $countryAlpha = BinProvider::getCountryAlpha2($this->bin);
        $coefficient = Countries::isEu($countryAlpha) ? 0.01 : 0.02;

        print_r([
            'amount' => $this->amount,
            'rate' => $rate,
            'amountFixed' => $amountFixed,
            'coefficient' => $coefficient,
            'countryAlpha' => $countryAlpha,
        ]);

        return bcmul($amountFixed, $coefficient, 2);
    }
}
