<?php

namespace services;

class BinProvider
{
    const URL = 'https://lookup.binlist.net/';

    private static array $countries = [];

    public static function getCountryAlpha2(string $bin): string
    {
        if(empty(self::$countries[$bin])){
            $result = file_get_contents(self::URL . $bin);
            if (!$result) {
                die("error!\n");
            }
            self::$countries[$bin] = json_decode($result);
        }

        return self::$countries[$bin]->country->alpha2;
    }
}
