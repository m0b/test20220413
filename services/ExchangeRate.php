<?php

namespace services;

class ExchangeRate
{
    const URL = 'http://api.exchangeratesapi.io/v1/latest';

    private static array $rates = [];

    public static function getRate(string $currency): float
    {
        $rates = self::getAllRates();
        if (!$rates) {
            die("error!\n");
        }

        return $rates[$currency] ?? 0;
    }

    private static function getAllRates(): array
    {
        if(empty(self::$rates)){
            $params = http_build_query([
                'access_key' => $_ENV['ACCESS_KEY']
            ]);
            $url = implode('?', [self::URL, $params]);
            $result = file_get_contents($url);
            self::$rates = @json_decode($result, true)['rates'];
        }

        return self::$rates;
    }
}
